#pragma once

class Camera
{
public:
    Camera();
    Camera(const Camera& other);
    ~Camera();

    void SetPosition(float x, float y, float z);
    void SetRotation(float x, float y, float z);

    DirectX::XMFLOAT3 GetPosition();
    DirectX::XMFLOAT3 GetRotation();

    void Render();
    void GetViewMatrix(DirectX::XMMATRIX& viewMatrix);

private:
    float positionX, positionY, positionZ;
    float rotationX, rotationY, rotationZ;
    DirectX::XMFLOAT4X4 viewMatrix;
};
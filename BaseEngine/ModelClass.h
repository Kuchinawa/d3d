#pragma once

class ModelClass
{
private:

    //Must match the layout in the ColorShaderContext
    struct VertexType
    {
        DirectX::XMFLOAT3 position;
        DirectX::XMFLOAT4 color;
    };

public:
    ModelClass();
    ModelClass(const ModelClass& other);
    ~ModelClass();

    bool Initialize(ComPtr<ID3D11Device> device);
    void Shutdown();
    void Render(ComPtr<ID3D11DeviceContext> deviceContext);

    int GetIndexCount();

private:
    bool InitializeBuffers(ComPtr<ID3D11Device> device);
    void ShutdownBuffers();
    void RenderBuffers(ComPtr<ID3D11DeviceContext> deviceContext);

private:
    ComPtr<ID3D11Buffer> vertexBuffer;
    ComPtr<ID3D11Buffer> indexBuffer;
    int vertexCount;
    int indexCount;
};
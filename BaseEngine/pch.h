#pragma once

#pragma message("Compiling precompiled headers")

//Pre-processing directives
#define WIN32_LEAN_AND_MEAN

//Windows Include
#include <Windows.h>
#include <wrl.h>

//Include DirectX Libraries
#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <DirectXMath.h>

//Port D3DXMATH -> DirectXMath
//https://msdn.microsoft.com/en-us/library/windows/desktop/ff729728.aspx


//Include common libraries
#include <memory>
#include <string>
#include <vector>

using std::unique_ptr;
using std::string;
using std::vector;
using Microsoft::WRL::ComPtr;
#include "pch.h"
#include "D3DContext.h"

using namespace DirectX;

D3DContext::D3DContext()
{
    swapChain = nullptr;
    device = nullptr;
    deviceContext = nullptr;
    renderTargetView = nullptr;
    depthStencilBuffer = nullptr;
    depthStencilState = nullptr;
    depthStencilView = nullptr;
    rasterState = nullptr;
}

D3DContext::D3DContext(const D3DContext& other)
{
}


D3DContext::~D3DContext()
{
}

bool D3DContext::Initialize(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen, float screenDepth, float screenNear)
{

    HRESULT result;
    ComPtr<IDXGIFactory> factory;
    ComPtr<IDXGIAdapter> adapter;
    ComPtr<IDXGIOutput> adapterOutput;

    unsigned int numModes, numerator, denominator;

    vector<DXGI_MODE_DESC> displayModeList;
    DXGI_ADAPTER_DESC adapterDesc;

    int error;

    DXGI_SWAP_CHAIN_DESC swapChainDesc;
    D3D_FEATURE_LEVEL featureLevel;
    ComPtr<ID3D11Texture2D> backBufferPtr;
    D3D11_TEXTURE2D_DESC depthBufferDesc;
    D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
    D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
    D3D11_RASTERIZER_DESC rasterDesc;
    D3D11_VIEWPORT viewport;

    float fieldOfView, screenAspect;

    //Store the vsync setting
    vsyncEnabled = vsync;

    //Create a DirectX graphics interface factory
    result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);

    if (FAILED(result))
    {
        return false;
    }

    //Use the factory to create an adapter for the primary graphics interface (video card)
    result = factory->EnumAdapters(0, &adapter);
    if (FAILED(result))
    {
        return false;
    }

    //Enumerate the primary adapter output (monitor)
    result = adapter->EnumOutputs(0, &adapterOutput);
    if (FAILED(result))
    {
        return false;
    }

    //Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor)
    result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, nullptr);
    if (FAILED(result))
    {
        return false;
    }

    //Create a list to hold all the possible display modes for this monitor/video card combination
    displayModeList.resize(numModes);

    if (displayModeList.size() == 0)
    {
        return false;
    }

    //Fill the display mode list structures
    result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList.data());

    if (FAILED(result))
    {
        return false;
    }

    //Now go through all the display modes and find the one that matches the screen width and height.
    //When a match is found store the numerator and denominator of the refresh rate for that monitor
    for (unsigned int i = 0; i < numModes; i++)
    {
        if (displayModeList[i].Width == (unsigned int)screenWidth)
        {
            if (displayModeList[i].Height == (unsigned int)screenHeight)
            {
                numerator = displayModeList[i].RefreshRate.Numerator;
                denominator = displayModeList.at(i).RefreshRate.Denominator;
            }
        }
    }

    //Get the adapter (video card) description
    result = adapter->GetDesc(&adapterDesc);
    if (FAILED(result))
    {
        return false;
    }

    //Store the dedicated video card memory in megabytes
    videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

    //Convert the name of the video card to a string and store it
    size_t stringLength;
    error = wcstombs_s(&stringLength, videoCardDescription, 128, adapterDesc.Description, 128);
    if (error != 0)
    {
        return false;
    }

    //Now that we have the numerator, denominator and video card info
    //we can release the structs and interfaces used to get that information

    //Release the display mode list
    displayModeList.clear();

    //Release the adapter output
    adapterOutput = nullptr;

    //Release the adapter
    adapter = nullptr;

    //Release the factory
    factory = nullptr;

    //Now that we have the info we need we create the swapchain
    //The swapchain contains the front and back buffer(s) to which the graphics are drawn
    //Generally you draw to the back buffer and switch it with the frontbuffer which display it to the screen

    //First we have to fill another description struct that describes the settings/features the swapchains is created with

    //Initialize the swap chain description
    ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

    //Set to a single back buffer
    swapChainDesc.BufferCount = 1;

    //Set the width and height of the back buffer
    swapChainDesc.BufferDesc.Width = screenWidth;
    swapChainDesc.BufferDesc.Height = screenHeight;

    //Set regular 32-bit (colors) surface for the back buffer
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

    //Set the refresh rate of the back buffer
    if (vsyncEnabled)
    {
        swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
        swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
    }
    else
    {
        swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
        swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    }

    //Set the usage of the back buffer
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

    //Set the handle for the window to render to
    swapChainDesc.OutputWindow = hwnd; //This is the same window we created earlier

    //Turn multisampleing off
    swapChainDesc.SampleDesc.Count = 1;
    swapChainDesc.SampleDesc.Quality = 0;

    //Set to full screen of windowed mode
    if (fullscreen)
    {
        swapChainDesc.Windowed = false;
    }
    else
    {
        swapChainDesc.Windowed = true;
    }

    //Set the scan line ordering and scaling to unspecified
    swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
    swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

    // Discard the back buffer contents after presenting.
    swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    // Don't set the advanced flags.
    swapChainDesc.Flags = 0;

    //Set the feature level to DirectX 11
    featureLevel = D3D_FEATURE_LEVEL_11_0;

    //Create the swap chain, Direct3D device and Direct3D device context
    result = D3D11CreateDeviceAndSwapChain(
        nullptr,
        D3D_DRIVER_TYPE_HARDWARE, //Driver type, if the pc doesnt have a directx graphics card we can use software but its slow
        nullptr,
        0,
        &featureLevel, //Directx version to use
        1,
        D3D11_SDK_VERSION,
        &swapChainDesc, //Our swapchain description struct
        &swapChain, //Our swapchain pointer
        &device, //Our device pointer
        nullptr,
        &deviceContext); //Our device context pointer

    if (FAILED(result))
    {
        return false;
    }

    //Get the pointer to the back buffer
    result = swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPtr);
    if (FAILED(result))
    {
        return false;
    }

    //Create the render target view with the back buffer pointer
    result = device->CreateRenderTargetView(backBufferPtr.Get(), nullptr, &renderTargetView);
    if (FAILED(result))
    {
        return false;
    }

    //Release pointer to the back buffer as we no longer need it
    backBufferPtr = nullptr;

    //Now we create a depth buffer, which is used for calculating the depth of a pixel
    //For example this can be used to calculate which texture is closest to the camera

    //Initialize the description of the depth buffer
    ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

    //Set up the description of the depth buffer
    depthBufferDesc.Width = screenWidth;
    depthBufferDesc.Height = screenHeight;
    depthBufferDesc.MipLevels = 1;
    depthBufferDesc.ArraySize = 1;
    depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthBufferDesc.SampleDesc.Count = 1;
    depthBufferDesc.SampleDesc.Quality = 0;
    depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    depthBufferDesc.CPUAccessFlags = 0;
    depthBufferDesc.MiscFlags = 0;

    //Create the texture for the depth buffer using the filled out description
    //We use the CreateTexture2D function because in the end the buffer is just a 2D texture
    //where all the polygons are sorted and rasterized on. This 2D texture will be drawn to the screen.
    result = device->CreateTexture2D(&depthBufferDesc, nullptr, &depthStencilBuffer);
    if (FAILED(result))
    {
        return false;
    }

    //Initialize the description of the stencil state
    ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

    //Set up the description of the stencil state
    depthStencilDesc.DepthEnable = true;
    depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
    depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

    depthStencilDesc.StencilEnable = true;
    depthStencilDesc.StencilReadMask = 0xFF;
    depthStencilDesc.StencilWriteMask = 0xFF;

    //Stencil operations if pixel is front-facing
    depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
    depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // Stencil operations if pixel is back-facing.
    depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
    depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
    depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

    // Create the depth stencil state.
    result = device->CreateDepthStencilState(&depthStencilDesc, &depthStencilState);
    if (FAILED(result))
    {
        return false;
    }

    // Set the depth stencil state.
    deviceContext->OMSetDepthStencilState(depthStencilState.Get(), 1);

    //Now we need to create the view of the depth stencil buffer
    //This enables direct3d to use the depth buffer as a depth stencil texture

    // Initailze the depth stencil view.
    ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

    // Set up the depth stencil view description.
    depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    depthStencilViewDesc.Texture2D.MipSlice = 0;

    // Create the depth stencil view.
    result = device->CreateDepthStencilView(depthStencilBuffer.Get(), &depthStencilViewDesc, &depthStencilView);
    if (FAILED(result))
    {
        return false;
    }

    //Now we can call OMSetRenderTargets
    //This will bind the render target view and the depth stencil buffer to the output render pipeline
    //This enables us to draw to the backbuffer of the swapchain we created earlier and swap that to the screen
    deviceContext->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());

    //Next we setup some extra functions that will give us some more control

    //The rasterizer state gives us control over how polygons are rendered.
    //eg. render in wireframe mode or draw both the front and back faces of polygons.
    //DirectX creates this by default but to gain control over it we need to create it ourselves

    //Setup the raster description which will determine how and what polygons will be drawn
    rasterDesc.AntialiasedLineEnable = false;
    rasterDesc.CullMode = D3D11_CULL_BACK;
    rasterDesc.DepthBias = 0;
    rasterDesc.DepthBiasClamp = 0.0f;
    rasterDesc.DepthClipEnable = true;
    rasterDesc.FillMode = D3D11_FILL_SOLID;
    rasterDesc.FrontCounterClockwise = false;
    rasterDesc.MultisampleEnable = false;
    rasterDesc.ScissorEnable = false;
    rasterDesc.SlopeScaledDepthBias = 0.0f;

    //Create the rasterizer state from the description we just filled out
    result = device->CreateRasterizerState(&rasterDesc, &rasterState);
    if (FAILED(result))
    {
        return false;
    }

    //Now set the rasterizer state
    deviceContext->RSSetState(rasterState.Get());

    //The viewport also needs to be setup so that Direct3D can map clip space coordinates to the render target space.
    //We set this to be the entire size of the window.

    // Setup the viewport for rendering.
    viewport.Width = (float)screenWidth;
    viewport.Height = (float)screenHeight;
    viewport.MinDepth = 0.0f;
    viewport.MaxDepth = 1.0f;
    viewport.TopLeftX = 0.0f;
    viewport.TopLeftY = 0.0f;

    // Create the viewport.
    deviceContext->RSSetViewports(1, &viewport);

    //Now we will create the projection matrix
    //The projection matrix is used to translate the 3D scene into the 2D viewport space that we previously created
    //We will pass this to our shaders later

    fieldOfView = (float)XM_PI / 4.0f;
    screenAspect = (float)screenWidth / (float)screenHeight;


    // Create the projection matrix for 3D rendering.
    XMStoreFloat4x4(&projectionMatrix, XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, screenNear, screenDepth));

    //We also need to create a world matrix, this matrix rotates, translates, and scales all the objects in 3D space
    //This will also be passed to our shaders later (for now this is just an identity matrix)
    XMStoreFloat4x4(&worldMatrix, XMMatrixIdentity());

    //Now we create an orthograpgic projection matrix, this is used for rendering 2D elements like UI elements.
    XMStoreFloat4x4(&orthoMatrix, XMMatrixOrthographicLH((float)screenWidth, (float)screenHeight, screenNear, screenDepth));

    return true;
}

void D3DContext::Shutdown()
{
    //Before shutting down set to windowed mode to prevent the swap chain from throwing an exception when releasing it.
    if (swapChain)
    {
        swapChain->SetFullscreenState(false, nullptr);
    }

    if (rasterState)
    {
        rasterState = nullptr;
    }

    if (depthStencilView)
    {
        depthStencilView = nullptr;
    }

    if (depthStencilState)
    {
        depthStencilState = nullptr;
    }

    if (depthStencilBuffer)
    {
        depthStencilBuffer = nullptr;
    }

    if (renderTargetView)
    {
        renderTargetView = nullptr;
    }

    if (deviceContext)
    {
        deviceContext = nullptr;
    }

    if (device)
    {
        device = nullptr;
    }

    if (swapChain)
    {
        swapChain = nullptr;
    }
}

///Resets the buffer so its ready to be drawn to
void D3DContext::BeginScene(float red, float green, float blue, float alpha)
{
    float color[4];

    // Setup the color to clear the buffer to.
    color[0] = red;
    color[1] = green;
    color[2] = blue;
    color[3] = alpha;

    // Clear the back buffer.
    deviceContext->ClearRenderTargetView(renderTargetView.Get(), color);

    // Clear the depth buffer.
    deviceContext->ClearDepthStencilView(depthStencilView.Get(), D3D11_CLEAR_DEPTH, 1.0f, 0);
}

///Display the scene (call after all the draws are completed)
void D3DContext::EndScene()
{
    // Present the back buffer to the screen since rendering is complete.
    if (vsyncEnabled)
    {
        // Lock to screen refresh rate.
        swapChain->Present(1, 0);
    }
    else
    {
        // Present as fast as possible.
        swapChain->Present(0, 0);
    }
}

ComPtr<ID3D11Device> D3DContext::GetDevice()
{
    return device;
}

ComPtr<ID3D11DeviceContext> D3DContext::GetDeviceContext()
{
    return deviceContext;
}

void D3DContext::GetProjectionMatrix(DirectX::XMMATRIX& projectionMatrix)
{
    projectionMatrix = XMLoadFloat4x4(&this->projectionMatrix);
}

void D3DContext::GetWorldMatrix(DirectX::XMMATRIX& worldMatrix)
{
    worldMatrix = XMLoadFloat4x4(&this->worldMatrix);
}

void D3DContext::GetOrthoMatrix(DirectX::XMMATRIX& orthoMatrix)
{
    orthoMatrix = XMLoadFloat4x4(&this->orthoMatrix);
}

void D3DContext::GetVideoCardInfo(char* cardName, int& memory)
{
    strcpy_s(cardName, 128, videoCardDescription);
    memory = videoCardMemory;
}


#include "pch.h"
#include "ModelClass.h"

using namespace DirectX;

ModelClass::ModelClass()
{
    vertexBuffer = nullptr;
    indexBuffer = nullptr;
}

ModelClass::ModelClass(const ModelClass & other)
{
}

ModelClass::~ModelClass()
{
}

bool ModelClass::Initialize(ComPtr<ID3D11Device> device)
{
    bool result;

    //Initialize the vertex and index buffers
    result = InitializeBuffers(device);
    if (!result)
    {
        return false;
    }

    return true;
}

void ModelClass::Shutdown()
{
    //Shutdown the vertex and index buffers
    ShutdownBuffers();
}

void ModelClass::Render(ComPtr<ID3D11DeviceContext> deviceContext)
{
    //Put the vertex and index buffers on the graphics pipeline to prepare them for drawing
    RenderBuffers(deviceContext);
}

int ModelClass::GetIndexCount()
{
    return indexCount;
}

bool ModelClass::InitializeBuffers(ComPtr<ID3D11Device> device)
{
    unique_ptr<VertexType[]> vertices;
    unique_ptr<unsigned long[]> indices;
    D3D11_BUFFER_DESC vertexBufferDesc;
    D3D11_BUFFER_DESC indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData;
    D3D11_SUBRESOURCE_DATA indexData;
    HRESULT result;

    //Usually we load a model from a model file but for this example we will just manually set some points


    //Set the number of vertices in the vertex array
    vertexCount = 3;

    //Set the number of indices in the index array
    indexCount = 3;

    //Create the vertex array
    vertices = unique_ptr<VertexType[]>(new VertexType[vertexCount]);
    if (!vertices)
    {
        return false;
    }

    //Create the index array
    indices = unique_ptr<unsigned long[]>(new unsigned long[indexCount]);
    if (!indices)
    {
        return false;
    }

    //Fill the vertex and index array with 3 points
    //Remember to do this clockwise, counter clock wise would make it face the oposite direction
    //and not draw them because of back face culling

    // Load the vertex array with data.
    vertices[0].position = XMFLOAT3(-1.0f, -1.0f, 0.0f);  // Bottom left.
    vertices[0].color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

    vertices[1].position = XMFLOAT3(0.0f, 1.0f, 0.0f);  // Top middle.
    vertices[1].color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

    vertices[2].position = XMFLOAT3(1.0f, -1.0f, 0.0f);  // Bottom right.
    vertices[2].color = XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f);

    // Load the index array with data.
    indices[0] = 0;  // Bottom left.
    indices[1] = 1;  // Top middle.
    indices[2] = 2;  // Bottom right.

    //Set up the description of the static vertex buffer
    vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * vertexCount; //Total model size
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER; //Vertex buffer type
    vertexBufferDesc.CPUAccessFlags = 0;
    vertexBufferDesc.MiscFlags = 0;
    vertexBufferDesc.StructureByteStride = 0;

    //Give the subresource structure a pointer to the vertex data
    vertexData.pSysMem = vertices.get();
    vertexData.SysMemPitch = 0;
    vertexData.SysMemSlicePitch = 0;

    //Now create the vertex buffer
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &vertexBuffer);
    if (FAILED(result))
    {
        return false;
    }

    //Set up the descriptionof the static index buffer
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
    indexBufferDesc.StructureByteStride = 0;

    //Give the subresource structure a pointer to the index data
    indexData.pSysMem = indices.get();
    indexData.SysMemPitch = 0;
    indexData.SysMemSlicePitch = 0;

    // Create the index buffer.
    result = device->CreateBuffer(&indexBufferDesc, &indexData, &indexBuffer);
    if (FAILED(result))
    {
        return false;
    }

    //Null the arrays now that the vertex and index buffers have been created and loaded.
    vertices = nullptr;
    indices = nullptr;

    return true;
}

void ModelClass::ShutdownBuffers()
{
    //Release the index buffer
    if (indexBuffer)
    {
        indexBuffer = nullptr;
    }
    if (vertexBuffer)
    {
        vertexBuffer = nullptr;
    }
}

///Sets the vertex and index buffers to active and defines how they should be drawn
void ModelClass::RenderBuffers(ComPtr<ID3D11DeviceContext> deviceContext)
{
    unsigned int stride;
    unsigned int offset;

    //Set vertex buffer stride and offset
    stride = sizeof(VertexType);
    offset = 0;

    //Set the vertex buffer to active in the input assembler so it can be rendered
    deviceContext->IASetVertexBuffers(0, 1, vertexBuffer.GetAddressOf(), &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
    deviceContext->IASetIndexBuffer(indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
    deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

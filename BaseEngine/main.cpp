#include "pch.h"
#include "SystemContext.h"

using std::make_unique;
using std::unique_ptr;

//Function prototypes
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

//Globals
static std::unique_ptr<SystemContext> sysCon = nullptr;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
    bool result;
    

    //Create the system object
    sysCon = make_unique<SystemContext>();

    if (system == nullptr)
    {
        return 0;
    }

    //Initialize and run the system object
    result = sysCon->Initialize(WndProc);

    if (result)
    {
        sysCon->Run();
    }

    //Shutdown and release the system object
    sysCon->Shutdown();
    sysCon = nullptr;

    return 0;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
    switch (umessage)
    {
        //Check if the window is being destroyed
    case WM_DESTROY:
    {
        PostQuitMessage(0);
        return 0;
    }
    //Check if the window is being closed
    case WM_CLOSE:
    {
        PostQuitMessage(0);
        return 0;
    }
    //All other messages pass to the message handler in the system class
    default:
    {
        return sysCon->MessageHandler(hwnd, umessage, wparam, lparam);
    }
    }
}
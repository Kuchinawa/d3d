#include "pch.h"
#include "ColorShaderContext.h"

ColorShaderContext::ColorShaderContext()
{
    vertexShader = nullptr;
    pixelShader = nullptr;
    layout = nullptr;
    matrixBuffer = nullptr;
}

ColorShaderContext::ColorShaderContext(const ColorShaderContext & other)
{
}

ColorShaderContext::~ColorShaderContext()
{
}

bool ColorShaderContext::Initialize(ComPtr<ID3D11Device> device, HWND hwnd)
{
    bool result;

    //Initialize the vertex and pixel shaders
    result = InitializeShader(device, hwnd, "colorVS.cso", "colorPS.cso");
    if (!result)
    {
        return false;
    }

    return true;
}

void ColorShaderContext::Shutdown()
{
    //Shutdown the vertex and pixel shaders as well as the related objects
    ShutdownShader();
}

bool ColorShaderContext::Render(ComPtr<ID3D11DeviceContext> deviceContext, int indexCount, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix)
{
    bool result;

    //Set the shader parameters that it will use for rendering
    result = SetShaderParameters(deviceContext, worldMatrix, viewMatrix, projectionMatrix);
    if (!result)
    {
        return false;
    }

    //Now render the prepared buffers with the shader
    RenderShader(deviceContext, indexCount);

    return true;
}

bool ColorShaderContext::InitializeShader(ComPtr<ID3D11Device> deviceContext, HWND hwnd, string vsFilename, string psFilename)
{
    HRESULT result;
    D3D11_INPUT_ELEMENT_DESC polygonLayout[2];
    D3D11_BUFFER_DESC matrixBufferDesc;

    //Load the vertex shader bytecode and create the vertex shader from it
    std::ifstream vsStream;
    size_t vsSize;
    unique_ptr<char[]> vsData = nullptr;

    vsStream.open(vsFilename, std::ifstream::in | std::ifstream::binary);

    if (vsStream.good())
    {
        vsStream.seekg(0, std::ios::end);
        vsSize = size_t(vsStream.tellg());
        vsData = unique_ptr<char[]>(new char[vsSize]);
        vsStream.seekg(0, std::ios::beg);
        vsStream.read(vsData.get(), vsSize);
        vsStream.close();

        result = deviceContext->CreateVertexShader(vsData.get(), vsSize, nullptr, &vertexShader);
        if (FAILED(result))
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    //Load the pixel shader bytecode and create the pixel shader from it
    std::ifstream psStream;
    size_t psSize;
    unique_ptr<char[]> psData = nullptr;

    psStream.open(psFilename, std::ifstream::in | std::ifstream::binary);

    if (psStream.good())
    {
        psStream.seekg(0, std::ios::end);
        psSize = size_t(psStream.tellg());
        psData = unique_ptr<char[]>(new char[psSize]);
        psStream.seekg(0, std::ios::beg);
        psStream.read(psData.get(), psSize);
        psStream.close();

        result = deviceContext->CreatePixelShader(psData.get(), psSize, nullptr, &pixelShader);
        if (FAILED(result))
        {
            return false;
        }
    }
    else
    {
        return false;
    }

    //Create the vertex input layout description
    //This setup needs to match the VertexType struct in the ModelClass and in the shader
    polygonLayout[0].SemanticName = "POSITION";
    polygonLayout[0].SemanticIndex = 0;
    polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
    polygonLayout[0].InputSlot = 0;
    polygonLayout[0].AlignedByteOffset = 0;
    polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    polygonLayout[0].InstanceDataStepRate = 0;
    
    polygonLayout[1].SemanticName = "COLOR";
    polygonLayout[1].SemanticIndex = 0;
    polygonLayout[1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    polygonLayout[1].InputSlot = 0;
    polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;//Makes sure the data is placed correctly in the buffer (put after the POSITION data)
    polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
    polygonLayout[1].InstanceDataStepRate = 0;

    //Now we create the input layout using the D3D device and above structs
    unsigned int numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

    //Create the vertex input layout
    result = deviceContext->CreateInputLayout(polygonLayout, numElements, vsData.get(), vsSize, &layout);

    if (FAILED(result))
    {
        return false;
    }

    //Setup the description of the dynamic matrix constant buffer that is in the the vertex shader
    matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC; //We will update it each frame
    matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType); //Size of the input data
    matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER; //Indicate we are making a constance buffer
    matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE; //Indicate that the CPU can write to this buffer
    matrixBufferDesc.MiscFlags = 0;
    matrixBufferDesc.StructureByteStride = 0;

    //Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class
    result = deviceContext->CreateBuffer(&matrixBufferDesc, nullptr, &matrixBuffer);
    if (FAILED(result))
    {
        return false;
    }

    return true;
}

void ColorShaderContext::ShutdownShader()
{
    //Null the matrix constant buffer
    if (matrixBuffer)
    {
        matrixBuffer = nullptr;
    }

    //Null the layout
    if (layout)
    {
        layout = nullptr;
    }

    //Null the pixel shader.
    if (pixelShader)
    {
        pixelShader = nullptr;
    }

    //Null the vertex shader.
    if (vertexShader)
    {
        vertexShader = nullptr;
    }
}

bool ColorShaderContext::SetShaderParameters(ComPtr<ID3D11DeviceContext> deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix)
{
    HRESULT result;
    D3D11_MAPPED_SUBRESOURCE mappedResource;
    unique_ptr<MatrixBufferType> dataPtr = nullptr;
    unsigned int bufferNumber;

    //Transpose the matrices to prepare them for the shader
    worldMatrix = DirectX::XMMatrixTranspose(worldMatrix);
    viewMatrix = DirectX::XMMatrixTranspose(viewMatrix);
    projectionMatrix = DirectX::XMMatrixTranspose(projectionMatrix);

    //Lock the constant buffer so it can be written to
    result = deviceContext->Map(matrixBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
    if (FAILED(result))
    {
        return false;
    }

    //Get a pointer to the data in the constant buffer
    dataPtr = unique_ptr<MatrixBufferType>((MatrixBufferType*)mappedResource.pData);

    //Copy the matrices into the constant buffer
    dataPtr->world = worldMatrix;
    dataPtr->view = viewMatrix;
    dataPtr->projection = projectionMatrix;

    dataPtr.release();

    // Unlock the constant buffer.
    deviceContext->Unmap(matrixBuffer.Get(), 0);

    //Set the position of the constant buffer in the vertex shader
    bufferNumber = 0;

    //Finaly set the constant buffer in the vertex shader with the updated values
    deviceContext->VSSetConstantBuffers(bufferNumber, 1, matrixBuffer.GetAddressOf());


    return true;
}

void ColorShaderContext::RenderShader(ComPtr<ID3D11DeviceContext> deviceContext, int indexCount)
{
    //Set the vertex input layout (lets the GPU know the format of the data in the vertex buffer)
    deviceContext->IASetInputLayout(layout.Get());

    //Set the vertex and pixel shaders that will be used to render this triangle
    deviceContext->VSSetShader(vertexShader.Get(), nullptr, 0);
    deviceContext->PSSetShader(pixelShader.Get(), nullptr, 0);

    //Render the triangle
    deviceContext->DrawIndexed(indexCount, 0, 0);    
}




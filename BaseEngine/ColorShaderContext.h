#pragma once

//Includes
#include <d3dcompiler.h>
#include <fstream>

class ColorShaderContext
{
private:
    struct MatrixBufferType
    {
        DirectX::XMMATRIX world;
        DirectX::XMMATRIX view;
        DirectX::XMMATRIX projection;
    };

public:
    ColorShaderContext();
    ColorShaderContext(const ColorShaderContext& other);
    ~ColorShaderContext();

    bool Initialize(ComPtr<ID3D11Device> device, HWND hwnd);
    void Shutdown();
    bool Render(ComPtr<ID3D11DeviceContext> deviceContext, int indexCount, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix,
        DirectX::XMMATRIX projectionMatrix);


private:
    bool InitializeShader(ComPtr<ID3D11Device> deviceContext, HWND hwnd, string vsFilename, string psFilename);
    void ShutdownShader();
    //void OutputShaderErrorMessage(ComPtr<ID3D10Blob> errorMessage, HWND hwnd, WCHAR* shaderFilename);


    bool SetShaderParameters(ComPtr<ID3D11DeviceContext> deviceContext, DirectX::XMMATRIX worldMatrix, DirectX::XMMATRIX viewMatrix, DirectX::XMMATRIX projectionMatrix);
    void RenderShader(ComPtr<ID3D11DeviceContext> deviceContext, int indexCount);

private:
    ComPtr<ID3D11VertexShader> vertexShader;
    ComPtr<ID3D11PixelShader> pixelShader;
    ComPtr<ID3D11InputLayout> layout;
    ComPtr<ID3D11Buffer> matrixBuffer;
};
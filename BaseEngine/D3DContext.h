#pragma once

class D3DContext
{
public:
    D3DContext();
    D3DContext(const D3DContext&);
    ~D3DContext();

    bool Initialize(int screenWidth, int screenHeight, bool vsync, HWND hwnd, bool fullscreen, float screenDepth, float screenNear);
    void Shutdown();

    ///Resets the buffer so its ready to be drawn to
    void BeginScene(float red, float green, float blue, float alpha);

    ///Display the scene (call after all the draws are completed)
    void EndScene();

    Microsoft::WRL::ComPtr<ID3D11Device> GetDevice();
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> GetDeviceContext();

    void GetProjectionMatrix(DirectX::XMMATRIX& projectionMatrix);
    void GetWorldMatrix(DirectX::XMMATRIX& worldMatrix);
    void GetOrthoMatrix(DirectX::XMMATRIX& orthoMatrix);

    void GetVideoCardInfo(char* cardName, int& memory);

private:
    bool vsyncEnabled;
    int videoCardMemory;
    char videoCardDescription[128];
    Microsoft::WRL::ComPtr<IDXGISwapChain> swapChain;
    Microsoft::WRL::ComPtr<ID3D11Device> device; //Pointer to the device we render to
    Microsoft::WRL::ComPtr<ID3D11DeviceContext> deviceContext; //Contains our rendering commands
    Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTargetView;
    Microsoft::WRL::ComPtr<ID3D11Texture2D> depthStencilBuffer;
    Microsoft::WRL::ComPtr<ID3D11DepthStencilState> depthStencilState;
    Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depthStencilView;
    Microsoft::WRL::ComPtr<ID3D11RasterizerState> rasterState;
    DirectX::XMFLOAT4X4 projectionMatrix;
    DirectX::XMFLOAT4X4 worldMatrix;
    DirectX::XMFLOAT4X4 orthoMatrix;




};
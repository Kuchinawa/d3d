#pragma once

//Project includes
#include "D3DContext.h"
#include "Camera.h"
#include "ModelClass.h"
#include "ColorShaderContext.h"


//Globals
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = true;
const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;

class GraphicsClass
{
public:
    GraphicsClass();
    GraphicsClass(const GraphicsClass&);
    ~GraphicsClass();

    bool Initialize(int, int, HWND);
    void Shutdown();
    bool Frame();

private:
    bool Render();

    unique_ptr<D3DContext> d3dContext;
    unique_ptr<Camera> camera;
    unique_ptr<ModelClass> model;
    unique_ptr<ColorShaderContext> colorShader;
};
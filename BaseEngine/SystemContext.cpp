#include "pch.h"
#include "SystemContext.h"

using std::make_shared;
using std::make_unique;
using std::unique_ptr;

SystemContext::SystemContext()
{
    m_Input = nullptr;
    m_Graphics = nullptr;
}

SystemContext::SystemContext(const SystemContext & other)
{
}

SystemContext::~SystemContext()
{
}

bool SystemContext::Initialize(WNDPROC WndProc)
{
    int screenWidth, screenHeight = 0;
    bool result;

    //Initialize the windows api
    InitializeWindows(screenWidth, screenHeight, WndProc);

    //Create the input object, this object will be used to handle reading the keyboard input from the user
    m_Input = make_unique<InputClass>();

    if (m_Input == nullptr)
    {
        return false;
    }

    //Initialize the input object
    m_Input->Initialize();

    //Create the graphics object, This object will handle rendering all the graphics for this application.
    m_Graphics = make_unique<GraphicsClass>();

    if (m_Graphics == nullptr)
    {
        return false;
    }

    //Initialize the graphics object
    result = m_Graphics->Initialize(screenWidth, screenHeight, m_hwnd);

    if (!result)
    {
        return false;
    }

    return true;
}

void SystemContext::Shutdown()
{
    //Release the graphics object
    if (m_Graphics != nullptr)
    {
        m_Graphics->Shutdown();
        m_Graphics.reset(nullptr);
    }

    //Release the input object
    if (m_Input != nullptr)
    {
        m_Input.reset(nullptr);
    }

    //Shutdown the window
    ShutdownWindows();
}

void SystemContext::Run()
{
    MSG msg;
    bool done, result;

    //Initialize the message structure
    ZeroMemory(&msg, sizeof(MSG));

    //Loop until there is a quit message from the window or the user.
    done = false;
    while (!done)
    {
        //Handle the windows messages
        if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        //If windows signals to end the application then exit out
        if (msg.message == WM_QUIT)
        {
            done = true;
        }
        else
        {
            //Otherwise do frame processing
            result = Frame();
            if (!result)
            {
                done = true;
            }
        }
    }
}

bool SystemContext::Frame()
{
    bool result;

    //Check if the user pressed escape and wants to exit the application
    if (m_Input->IsKeyDown(VK_ESCAPE))
    {
        return false;
    }

    //Do the frame processing for the graphics object
    result = m_Graphics->Frame();

    if (!result)
    {
        return false;
    }

    return true;
}

///<summary>Accepts the windows system messages and handles them. eg keyboard presses.</summary>
LRESULT SystemContext::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
    switch (umsg)
    {
        //Check if a key has been pressed on the keyboard
    case WM_KEYDOWN:
    {
        //If a key is pressed send it to the input object so it can record that state
        m_Input->KeyDown((unsigned int)wparam);
        return 0;
    }
    //Check if a key has been released on the keyboard
    case WM_KEYUP:
    {
        //If a key is released then send it to the input object so it can unset the state for that key
        m_Input->KeyUp((unsigned int)wparam);
        return 0;
    }
    //Any other messages send to the default message handler as our application won't make use of them
    default:
    {
        return DefWindowProc(hwnd, umsg, wparam, lparam);
    }

    }
}

void SystemContext::InitializeWindows(int& screenWidth, int& screenHeight, WNDPROC WndProc)
{
    WNDCLASSEX wc; //Struct that describes our window
    DEVMODE dmScreenSettings; //Struct that describes our screen device
    int posX, posY;

    //Get the instance of this application
    m_hinstance = GetModuleHandle(nullptr);

    //Give the application a name
    m_applicationName = L"Engine";

    //Setup the windows class with default settings
    wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = m_hinstance;
    wc.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
    wc.hIconSm = wc.hIcon;
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wc.lpszMenuName = nullptr;
    wc.lpszClassName = m_applicationName;
    wc.cbSize = sizeof(WNDCLASSEX);

    //Register the window class
    RegisterClassEx(&wc);

    //Determine the resolution of the clients desktop screen.
    screenWidth = GetSystemMetrics(SM_CXSCREEN);
    screenHeight = GetSystemMetrics(SM_CYSCREEN);

    //Setup the screen settings depending on whether it is running in full screen or in windowed mode.
    if (FULL_SCREEN)
    {
        //If full screen set the screen to maximum size of the users desktop and 32bit.
        memset(&dmScreenSettings, 0, sizeof(dmScreenSettings)); //Reset the struct to 0
        dmScreenSettings.dmSize = sizeof(dmScreenSettings);
        dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth; //Set display width
        dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight; //Set display height
        dmScreenSettings.dmBitsPerPel = 32; //Set the color resolution in bits per pixel
        dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT; //Tells the device which struct fields have been set

        //Change the display settings using our struct and the fullscreen flag
        ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

        //Set the position of the window to the top left corner
        posX = posY = 0;
    }
    else
    {
        //If windowed then set it to 800x600
        screenWidth = 800;
        screenHeight = 600;

        //Place the window in the middle of the screen
        posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
        posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
    }

    //Create the window with the screen settings and get the handle to it
    m_hwnd = CreateWindowEx(
        WS_EX_APPWINDOW, //Sets the window on the taskbar
        m_applicationName, //Set the window class name
        m_applicationName,//Set the window name
        WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP, //Set flags for the window style (borderless)
        //WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_OVERLAPPEDWINDOW, //Border + buttons + resize
        posX, posY, //Set the position of the window
        screenWidth, screenHeight, //Set the size of the window
        nullptr, //Parent parameter is empty
        nullptr, //Menu parameter is empty
        m_hinstance, //Link this to the window
        nullptr);

    //Bring the window up on the screen and set it as main focus
    ShowWindow(m_hwnd, SW_SHOW);
    SetForegroundWindow(m_hwnd);
    SetFocus(m_hwnd);

    //Hide the mouse cursor
    ShowCursor(false);
}

void SystemContext::ShutdownWindows()
{
    //Show the mouse cursor
    ShowCursor(true);

    //Fix the display settings if leaving full screen mode
    if (FULL_SCREEN)
    {
        ChangeDisplaySettings(nullptr, 0);
    }

    //Remove the window
    DestroyWindow(m_hwnd);
    m_hwnd = nullptr;

    //Remove the application instance
    UnregisterClass(m_applicationName, m_hinstance);
    m_hinstance = nullptr;
}


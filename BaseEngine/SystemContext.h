#pragma once



//Project includes
#include "InputClass.h"
#include "GraphicsClass.h"

class SystemContext
{
public:
    SystemContext();
    SystemContext(const SystemContext&);
    ~SystemContext();

    bool Initialize(WNDPROC);
    void Shutdown();
    void Run();

    ///<summary>Accepts the windows system messages and handles them. eg keyboard presses.</summary>
    LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:
    bool Frame();
    void InitializeWindows(int&, int&, WNDPROC);
    void ShutdownWindows();

    LPCWSTR m_applicationName;
    HINSTANCE m_hinstance;
    HWND m_hwnd;

    std::unique_ptr<InputClass> m_Input;
    std::unique_ptr<GraphicsClass> m_Graphics;
    
};
#include "pch.h"
#include "InputClass.h"

InputClass::InputClass()
{
}

InputClass::InputClass(const InputClass& other)
{
}

InputClass::~InputClass()
{

}

void InputClass::Initialize()
{
    //Initialize all the keys to being released and not pressed
    for (int i = 0; i < 256; i++)
    {
        m_keys[i] = false;
    }
}

void InputClass::KeyDown(unsigned int input)
{
    //If a key is pressed then save that state in the key array.
    m_keys[input] = true;
}

void InputClass::KeyUp(unsigned int input)
{
    //If a key is released then clear that state in the key array.
    m_keys[input] = false;
}

bool InputClass::IsKeyDown(unsigned int key)
{
    //Return the key state (Pressed/Not Pressed)
    return m_keys[key];
}
#include "pch.h"
#include "GraphicsClass.h"


GraphicsClass::GraphicsClass()
{
    d3dContext = nullptr;
    camera = nullptr;
    model = nullptr;
    colorShader = nullptr;
}

GraphicsClass::GraphicsClass(const GraphicsClass& other)
{
}

GraphicsClass::~GraphicsClass()
{
}

bool GraphicsClass::Initialize(int screenWidth, int screenHeight, HWND hwnd)
{
    bool result;

    // Create the Direct3D object.
    d3dContext = std::make_unique<D3DContext>();
    if (!d3dContext)
    {
        return false;
    }

    // Initialize the Direct3D object.
    result = d3dContext->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
    if (!result)
    {
        MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
        return false;
    }

    //Create the camera object
    camera = std::make_unique<Camera>();
    if (!camera)
    {
        return false;
    }

    //Set the initial position of th camera
    camera->SetPosition(0.0f, 0.0f, -5.0f);

    //Create the model object
    model = std::make_unique<ModelClass>();
    if (!model)
    {
        return false;
    }


    // Initialize the model object.
    result = model->Initialize(d3dContext->GetDevice());
    if (!result)
    {
        MessageBox(hwnd, L"Could not initialize the model object.", L"Error", MB_OK);
        return false;
    }

    // Create the color shader object.
    colorShader = std::make_unique<ColorShaderContext>();
    if (!colorShader)
    {
        return false;
    }


    // Initialize the color shader object.
    result = colorShader->Initialize(d3dContext->GetDevice(), hwnd);
    if (!result)
    {
        MessageBox(hwnd, L"Could not initialize the color shader object.", L"Error", MB_OK);
        return false;
    }

    return true;
}

void GraphicsClass::Shutdown()
{
    //Shutdown & Null the color shader object.
    if (colorShader)
    {
        colorShader->Shutdown();
        colorShader = nullptr;
    }

    //Shutdown & Null the model object.
    if (model)
    {
        model->Shutdown();
        model = nullptr;
    }

    //Null the camera object.
    if (camera)
    {
        camera = nullptr;
    }

    //Shutdown & Null the D3D context
    if (d3dContext)
    {
        d3dContext->Shutdown();
        d3dContext = nullptr;
    }
}


bool GraphicsClass::Frame()
{
    bool result;


    // Render the graphics scene.
    result = Render();
    if (!result)
    {
        return false;
    }

    return true;
}


bool GraphicsClass::Render()
{
    DirectX::XMMATRIX worldMatrix, viewMatrix, projectionMatrix;
    bool result;

    //Clear the buffers to begin the scene
    d3dContext->BeginScene(0.0f, 0.0f, 0.8f, 1.0f);

    //Generate the view matrix based on the camera's position
    camera->Render();

    //Get the world, view and projection matrices from the camera and d3d objects
    d3dContext->GetWorldMatrix(worldMatrix);
    camera->GetViewMatrix(viewMatrix);
    d3dContext->GetProjectionMatrix(projectionMatrix);

    //Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing
    model->Render(d3dContext->GetDeviceContext());

    //Render the model using the color shader
    result = colorShader->Render(d3dContext->GetDeviceContext(), model->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix);
    if (!result)
    {
        return false;
    }

    // Present the rendered scene to the screen.
    d3dContext->EndScene();

    return true;
}